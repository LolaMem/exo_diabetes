# PROJECT DIABETE DATA

Proposer un modèle de données pertinent à partir de données brutes.

* Contexte du projet
A partir de 4 jeux de données (montrés plus tard: A, B, D & D):

proposez un MLD qui permettrait de stocker à minima les données nécessaires pour savoir si une personne est ou non diabetique. Pour chaque attribut que va stocker la BDD, justifier sa présence en montrant la corrélation entre ses valeurs et l’état de santé du patient.

* Livrables
repo GIT.
- le ou les notebook avec les justifications: Justification, correlation-graph entre diabetes et les parametres pertinentes
- le MLD_Model logic des données

## ANALYSE DES JEUX DE DONNEES

* A_Diabetes Dataset - Pima Indians
[link](https://www.kaggle.com/datasets/nancyalaswad90/review/data?select=diabetes.csv)
All patients here are females at least 21 years old of Pima Indian heritage.2 (Arizona native Americans)
A_diabetes.csv 768 lignes
Columns: Pregnancies,Glucose,BloodPressure,SkinThickness,Insulin,BMI,DiabetesPedigreeFunction,Age,Outcome

 0   Pregnancies: Number of
 1   Glucose                   768 non-null    int64  
 2   BloodPressure: values 0 and 24 to 122, mmHg 
 3   SkinThickness             768 non-null    int64  
 4   Insulin                   768 non-null    int64  
 5   BMI                       768 non-null    float64
 6   DiabetesPedigreeFunction: is a function that scores the probability of diabetes based on family history, with a realistic range of 0.08 to 2.42.
 7   Age: 21 - 81 
 8   Outcome: the target variable (predicted), 0 represents non diabetes, and 1 represents those with diabetes 

* B_Diabetes
[Link](https://www.kaggle.com/datasets/imtkaggleteam/diabetes)
Several hundred rural African-American patients were included. The diabetes.csv file contains the raw data of all patients, including those with missing data
B_diabetes.csv  403 lignes
id,chol,stab.glu,hdl,ratio,glyhb,location,age,gender,height,weight,frame,bp.1s,bp.1d,bp.2s,bp.2d,waist,hip,time.ppn


* C_Diabetes Health Indicators Dataset
[Link](https://www.kaggle.com/datasets/alexteboul/diabetes-health-indicators-dataset)
2015_This original dataset contains responses from 441,455 individuals and has 330 features. These features are either questions directly asked of participants, or calculated variables based on individual participant responses.
Behavioral Risk Factor Surveillance System (BRFSS)
Centers for Disease Control and Prevention (CDC)
3 csv files:
1. diabetes _ 012 _ health _ indicators _ BRFSS2015.csv is a clean dataset of 253,680 survey responses to the CDC's BRFSS2015. The target variable Diabetes_012 has 3 classes. 0 is for no diabetes or only during pregnancy, 1 is for prediabetes, and 2 is for diabetes. There is class imbalance in this dataset. This dataset has 21 feature variables

2. diabetes _ binary _ 5050split _ health _ indicators _ BRFSS2015.csv is a clean dataset of 70,692 survey responses to the CDC's BRFSS2015. It has an equal 50-50 split of respondents with no diabetes and with either prediabetes or diabetes. The target variable Diabetes_binary has 2 classes. 0 is for no diabetes, and 1 is for prediabetes or diabetes. This dataset has 21 feature variables and is balanced.

3. diabetes _ binary _ health _ indicators _ BRFSS2015.csv is a clean dataset of 253,680 survey responses to the CDC's BRFSS2015. The target variable Diabetes_binary has 2 classes. 0 is for no diabetes, and 1 is for prediabetes or diabetes. This dataset has 21 feature variables and is not balanced.

Risk factors

[Machine Learning model example](https://www.cdc.gov/pcd/issues/2019/19_0109.htm)

* D_Diabetes prediction dataset
[Link](https://www.kaggle.com/datasets/iammustafatz/diabetes-prediction-dataset)
is a collection of medical and demographic data from patients. 
Data includes features such as age, gender, body mass index (BMI), hypertension, heart disease, smoking history, HbA1c level, and blood glucose level, and the target variable being predicted, with values of 1 for diabetes and 0 absence de diabetes
D_diabetes_prediction_dataset.csv   100.000 Lignes
gender,age,hypertension,heart_disease,smoking_history,bmi,HbA1c_level,blood_glucose_level,diabetes

## ANALYSIS OF PARAMETERS AND JUSTIFICATION

### BODY MASS INDEX, BMI
BMI = kg/m2 where kg is a person's weight in kilograms and m2 is their height in metres squared
![Alt text](BMI.png)

### GLUCOSE LEVEL
There are different ways to measure the level of glucose in blood

* Result  A1C or HbA1c: average blood glucose for the past two to three months
Normal          less than 5.7%
Prediabetes     5.7% to 6.4%
Diabetes        6.5% or higher

* Result  Fasting Plasma Glucose (FFPG): glucose level after 8h fasting
Normal          less than 100 mg/dL or less than 5.7 mmol/L
Prediabetes     100 mg/dl to 125 mg/dL or 5.7 to 6.4 mmol/L
Diabetes        126 mg/dL or higher or 6.4 mmol/L or higher

* Result  Oral Glucose Tolerance Test (OGTT): is a two-hour test that checks your blood glucose levels before and two hours after you drink a special sweet drink
Normal          less than 140 mg/dL or less than 7.8 mmol/L
Prediabetes     140 to 199 mg/dL or 7.8 to 11.0 mmol/L
Diabetes        200 mg/dL or higher or 11.1 mmol/L or higher









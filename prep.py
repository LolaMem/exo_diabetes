
# import pandas as pd
# import plotly.express as px
# import plotly.graph_objects as go
# from plotly.subplots import make_subplots
# import warnings
# warnings.filterwarnings("ignore")
# from dataprep.eda import create_report

# df_A = pd.read_csv("A_diabetes.csv")

from dataprep.eda import plot, create_report
from dataprep.datasets import load_dataset
import numpy as np
import pandas as pd
#df = load_dataset('adult')
#create_report(df).show_browser()

df_A = pd.read_csv("A_diabetes.csv")
#create_report(df_A).show_browser()
report_A = create_report(df_A)
report_A
report_A.save('Report_A')

df_B = pd.read_csv("B_diabetes.csv")
report_B = create_report(df_B)
report_B
report_B.save('Report_B')